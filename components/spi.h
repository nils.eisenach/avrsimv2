#ifndef _SPI_H
#define _SPI_H


#define SPI_SLAVE_BITBANG false


#if SPI_SLAVE_BITBANG

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <simavr/sim_avr.h>
#include <simavr/sim_irq.h>

enum {
    IRQ_SPI_SLAVE_CLK = 0,
    IRQ_SPI_SLAVE_SI,
    IRQ_SPI_SLAVE_SO,
    IRQ_SPI_SLAVE_CS,
    IRQ_SPI_SLAVE_COUNT
};
typedef struct spi_slave_t spi_slave_t;
struct spi_slave_t {
    avr_t *avr;
    avr_irq_t *irq;
    const char *name;
    bool polarity: 1; // 0: idle low, 1: idle high
    bool phase: 1; // 0: leading, 1: trailing
    
    bool si_state: 1;
    bool so_state: 1;
    bool selected: 1;
    
    size_t bit_counter; // number of received bits since cs

    uint8_t word;
    
    void *param;
    uint8_t (*onPreWord)(size_t wordNumber, void *param);
    void (*onWord)(size_t wordNumber, uint8_t word, void *param);
};


void spi_slave_init(spi_slave_t *s, avr_t *avr, const char *name);
void spi_slave_connect(spi_slave_t *s, avr_irq_t *clk, avr_irq_t *si, avr_irq_t *so, avr_irq_t *cs);

#else

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <simavr/sim_avr.h>
#include <simavr/sim_irq.h>

enum {
    IRQ_SPI_SLAVE_MISO = 0,
    IRQ_SPI_SLAVE_MOSI,
    IRQ_SPI_SLAVE_CS,
    IRQ_SPI_SLAVE_COUNT
};
typedef struct spi_slave_t spi_slave_t;
struct spi_slave_t {
    avr_t *avr;
    avr_irq_t *irq;
    
    bool selected: 1;
    size_t word_counter; // number of received words since cs
    
    void *param;
    uint8_t (*onPreWord)(size_t wordNumber, void *param);
    void (*onWord)(size_t wordNumber, uint8_t word, void *param);
};


void spi_slave_init(spi_slave_t *s, avr_t *avr);
void spi_slave_connect(spi_slave_t *s, avr_irq_t *miso, avr_irq_t *mosi, avr_irq_t *cs);



#endif


#endif //_SPI_H