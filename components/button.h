#ifndef _BUTTON_H
#define _BUTTON_H

#include <stdbool.h>
#include <simavr/sim_avr.h>
#include <simavr/sim_irq.h>

enum {
    IRQ_BUTTON_OUT = 0,
    IRQ_BUTTON_COUNT
};

typedef struct button_t {
    avr_t *avr;
    avr_irq_t *irq;
} button_t;

void button_init(button_t *b, avr_t *avr);
void button_connect(button_t *b, avr_irq_t *out);
/**
 * Presses the button for \p duration_usec useconds.
 * If \p duration_usec is \c 0 the button gets pressed indefinitely.
 */
void button_press(button_t *b, uint32_t duration_usec);
/**
 * Same as button_press(b, 0)
 */
void button_hold(button_t *b);
/**
 * Releases the button immediately
 */
void button_release(button_t *b);

#endif //_BUTTON_H
