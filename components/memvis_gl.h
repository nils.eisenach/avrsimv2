#ifndef _MEMVIS_GL_H
#define _MEMVIS_GL_H

#include <stdlib.h>
#include <stdint.h>
#include "../common/gl_utils.h"


typedef GLcolor32 memvis_gl_colors_t[0xFF + 1];
typedef struct {
    GLfloat v[3];
} GLfloat3;
typedef struct {
    uint8_t *mem;
    size_t mem_size;
    size_t cols;
    struct {
        float width, height;
    } cell;
    memvis_gl_colors_t colors;
    GLuint vbo[2];
    size_t bufferSize;
    GLfloat3 *tmp;
} memvis_gl_t;


GLvec2f memvis_gl_getSize(memvis_gl_t *m);
void memvis_gl_init(memvis_gl_t *m);
void memvis_gl_free(memvis_gl_t *m); // TODO call
void memvis_gl_draw(memvis_gl_t *m);

typedef enum {
    MVGL_C_GRAD_BW,
    MVGL_C_GRAD_SPECTRUM
} memvis_gl_colors_style_t;
void memvis_gl_genColors(memvis_gl_colors_t *result, memvis_gl_colors_style_t style);

#endif //_MEMVIS_GL_H
