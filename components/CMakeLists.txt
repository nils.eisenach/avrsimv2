cmake_minimum_required(VERSION 3.9)



add_library(avrsimv2-components
        button.c
        lcd.c
        lcd_gl.c
        led.c
        led_gl.c
        memvis_gl.c
        spi.c
        sram_23LC1024.c)
set_property(TARGET avrsimv2-components PROPERTY POSITION_INDEPENDENT_CODE ON)
target_include_directories(avrsimv2-components PRIVATE ${INCLUDE_DIRS})
target_link_directories(avrsimv2-components PRIVATE ${LIBRARY_DIRS})
target_link_libraries(avrsimv2-components avrsimv2)