#ifndef _BOARD_XML_BUTTON_C
#define _BOARD_XML_BUTTON_C

#include "board_xml_common.c"


#include "../components/button.h"


typedef struct {
    button_t button;
    board_xml_irq_t out;
    board_xml_key_t key;
} board_xml_button_t;


static BOARD_XML_COMP_INIT(board_xml_button_comp_init, data, board, avr) {
    board_xml_data_cast(board_xml_button_t, btn, data);
    button_init(&btn->button, avr);
    button_connect(&btn->button, board_xml_irq_get(avr, btn->out));
}

static BOARD_XML_COMP_ONKEY(board_xml_button_comp_onKey, data, board, key, scancode, action, mods) {
    board_xml_data_cast(board_xml_button_t, btn, data);
    if (key == btn->key.code) {
        if (action == GLFW_PRESS) button_hold(&btn->button);
        else if (action == GLFW_RELEASE) button_release(&btn->button);
    }
}


static void board_xml_button_parse(board_xml_t *board, xmlNode *node, board_xml_comp_t *comp) {
    comp->init = board_xml_button_comp_init;
    comp->onKey = board_xml_button_comp_onKey;
    board_xml_button_t *btn = comp->data = calloc(1, sizeof(board_xml_button_t)); // free
    btn->out = board_xml_irq_parse(xmlUtilsFindChild(node, "out"));
    btn->key = board_xml_key_parse(xmlUtilsFindChild(node, "key"));
}




#endif // _BOARD_XML_BUTTON_C