#ifndef _LINKED_LIST_H
#define _LINKED_LIST_H

#include <stdlib.h>
#include <stdbool.h>


#define NOOP_FUNC_NAME_(L) NOOP_FUNC_##L
#define NOOP_FUNC_NAME(L) NOOP_FUNC_NAME_(L)

#define NOOP_FUNC void NOOP_FUNC_NAME(__LINE__) (void)


// SYMBOLS

#define _LL(id)             ll__##id
#define _LL__(id)           ll__##id##__
#define _LL_TYPE(id)        ll__##id##_T
#define _LL_NODE(id)        ll__##id##_node
#define _LL_NODE__(id)      ll__##id##_node__
#define _LL_GETNODE(id)     ll__##id##_getNode
#define _LL_INSERT(id)      ll__##id##_insert
#define _LL_ADD(id)         ll__##id##_add
#define _LL_GET(id)         ll__##id##_get
#define _LL_REMOVE(id)      ll__##id##_remove
#define _LL_CLEAR(id)       ll__##id##_clear
#define _LL_ENUMERATOR(id)  ll__##id##_enumerator
#define _LL_ENUM(id)        ll__##id##_enum
#define _LL_ENUM__(id)      ll__##id##_enum__
#define _LL_ENUM_NEXT(id)   ll__##id##_enum_next

#define _LL_NOOP    NOOP_FUNC
#define _LL_head    _head
#define _LL_tail    _tail
#define _LL_ll      _ll
#define _LL_node    _node

// DEF (public)

#define _LL_DEF(id) struct _LL__(id) { \
    _LL_NODE(id) *_LL_head; \
    _LL_NODE(id) *_LL_tail; \
    size_t count; \
    _LL_TYPE(id) def;\
}
#define _LL_DEF_NODE(id) struct _LL_NODE__(id) { \
    _LL_NODE(id) *prev; \
    _LL_NODE(id) *next; \
    _LL_TYPE(id) value; \
}
#define _LL_DEF_ENUM(id) struct _LL_ENUM__(id) { \
    _LL(id) *_LL_ll; \
    _LL_NODE(id) *_LL_node; \
    _LL_TYPE(id) *current; \
    size_t index; \
}

// DECL

#define _LL_DECL(id)                typedef struct _LL__(id) _LL(id); _LL_DEF(id)
#define _LL_DECL_TYPE(id, T)        typedef T _LL_TYPE(id)
#define _LL_DECL_NODE(id)           typedef struct _LL_NODE__(id) _LL_NODE(id); _LL_DEF_NODE(id)
#define _LL_DECL_GETNODE(id)        _LL_NODE(id) *_LL_GETNODE(id)(_LL(id) *ll, size_t index)
#define _LL_DECL_INSERT(id)         void _LL_INSERT(id)(_LL(id) *ll, size_t index, _LL_TYPE(id) v)
#define _LL_DECL_ADD(id)            void _LL_ADD(id)(_LL(id) *ll, _LL_TYPE(id) v)
#define _LL_DECL_GET(id)            _LL_TYPE(id) _LL_GET(id)(_LL(id) *ll, size_t index)
#define _LL_DECL_REMOVE(id)         _LL_TYPE(id) _LL_REMOVE(id)(_LL(id) *ll, size_t index)
#define _LL_DECL_CLEAR(id)          void _LL_CLEAR(id)(_LL(id) *ll)
#define _LL_DECL_ENUMERATOR(id)     _LL_ENUM(id) _LL_ENUMERATOR(id)(_LL(id) *ll)
#define _LL_DECL_ENUM(id)           typedef struct _LL_ENUM__(id) _LL_ENUM(id); _LL_DEF_ENUM(id)
#define _LL_DECL_ENUM_NEXT(id)      bool _LL_ENUM_NEXT(id)(_LL_ENUM(id) *e)

// DEF (private)

#define _LL_DEF_GETNODE(id) _LL_DECL_GETNODE(id) { \
    if (index >= ll->count) return NULL; \
    _LL_NODE(id) *node; \
    if (index < ll->count / 2) { \
        node = ll->_LL_head; \
        while (index > 0) { \
            node = node->next; \
            index--; \
        } \
    } else { \
        node = ll->_LL_tail; \
        index = ll->count - index - 1; \
        while (index > 0) { \
            node = node->prev; \
            index--; \
        } \
    } \
    return node; \
} _LL_NOOP
#define _LL_DEF_INSERT(id) _LL_DECL_INSERT(id) { \
    _LL_NODE(id) *new = malloc(sizeof(_LL_NODE(id))); \
    new->value = v; \
    if (index > ll->count) return; \
    if (index < ll->count) { \
        _LL_NODE(id) *node = _LL_GETNODE(id)(ll, index); \
        new->prev = node->prev; \
        new->next = node; \
        if (ll->_LL_head == node) ll->_LL_head = new; \
    } else { \
        new->prev = ll->_LL_tail; \
        new->next = NULL; \
        if (ll->_LL_head == NULL) ll->_LL_head = new; \
        ll->_LL_tail = new; \
    } \
    if (new->prev) new->prev->next = new; \
    if (new->next) new->next->prev = new; \
    ll->count++; \
} _LL_NOOP
#define _LL_DEF_ADD(id) _LL_DECL_ADD(id) { \
    _LL_INSERT(id)(ll, ll->count, v); \
} _LL_NOOP
#define _LL_DEF_GET(id) _LL_DECL_GET(id) { \
    _LL_NODE(id) *node = _LL_GETNODE(id)(ll, index); \
    if (!node) return ll->def; \
    return node->value; \
} _LL_NOOP
#define _LL_DEF_REMOVE(id) _LL_DECL_REMOVE(id) { \
    _LL_NODE(id) *node = _LL_GETNODE(id)(ll, index); \
    if (!node) return ll->def; \
    if (ll->_LL_head == node) ll->_LL_head = node->next; \
    if (ll->_LL_tail == node) ll->_LL_tail = node->prev; \
    if (node->prev) node->prev->next = node->next; \
    if (node->next) node->next->prev = node->prev; \
    ll->count--; \
    _LL_TYPE(id) removed = node->value; \
    free(node); \
    return removed; \
} _LL_NOOP
#define _LL_DEF_CLEAR(id) _LL_DECL_CLEAR(id) { \
    _LL_NODE(id) *node = ll->_LL_head; \
    while (node) { \
        _LL_NODE(id) *next = node->next; \
        free(node); \
        node = next; \
    } \
    ll->count = 0; \
} _LL_NOOP
#define _LL_DEF_ENUMERATOR(id) _LL_DECL_ENUMERATOR(id) { \
    return (_LL_ENUM(id)) { ._ll = ll, ._node = NULL, .current = NULL, .index = 0 }; \
} _LL_NOOP
#define _LL_DEF_ENUM_NEXT(id) _LL_DECL_ENUM_NEXT(id) { \
    if (e->index >= e->_LL_ll->count) return false; \
    if (!e->_LL_node && e->index != 0) return false; \
    if (!e->_LL_node) { \
        e->_LL_node = e->_LL_ll->_LL_head; \
        e->current = &e->_LL_node->value; \
        e->index = 0; \
    } else { \
        e->_LL_node = e->_LL_node->next; \
        e->current = &e->_LL_node->value; \
        e->index++; \
    } \
    return true; \
} _LL_NOOP

// API

#define LL_DECL_TYPES(id, T) \
    _LL_DECL_TYPE(id, T); \
    _LL_DECL_NODE(id); \
    _LL_DECL(id); \
    _LL_DECL_ENUM(id)
#define LL_DECL_FUNCS(id) \
    _LL_DECL_INSERT(id); \
    _LL_DECL_ADD(id); \
    _LL_DECL_GET(id); \
    _LL_DECL_REMOVE(id); \
    _LL_DECL_CLEAR(id); \
    _LL_DECL_ENUMERATOR(id); \
    _LL_DECL_ENUM_NEXT(id)
#define LL_DECL(id, T) \
    LL_DECL_TYPES(id, T); \
    LL_DECL_FUNCS(id)
#define LL_DEF(id) \
    static _LL_DECL_GETNODE(id); \
    static _LL_DEF_GETNODE(id); \
    _LL_DEF_INSERT(id); \
    _LL_DEF_ADD(id); \
    _LL_DEF_GET(id); \
    _LL_DEF_REMOVE(id); \
    _LL_DEF_CLEAR(id); \
    _LL_DEF_ENUMERATOR(id); \
    _LL_DEF_ENUM_NEXT(id)
#define LL(id)                      _LL(id)
#define LL_ENUM(id)                 _LL_ENUM(id)
#define ll_instance(id, default)    ((_LL(id)) {._LL_head = NULL, ._LL_tail = NULL, .count = 0, .def = (default)})
#define ll_insert(id, ll, ...)      _LL_INSERT(id)      (ll, ##__VA_ARGS__)
#define ll_add(id, ll, ...)         _LL_ADD(id)         (ll, ##__VA_ARGS__)
#define ll_get(id, ll, ...)         _LL_GET(id)         (ll, ##__VA_ARGS__)
#define ll_remove(id, ll, ...)      _LL_REMOVE(id)      (ll, ##__VA_ARGS__)
#define ll_clear(id, ll, ...)       _LL_CLEAR(id)       (ll, ##__VA_ARGS__)
#define ll_enumerator(id, ll, ...)  _LL_ENUMERATOR(id)  (ll, ##__VA_ARGS__)
#define ll_enum_next(id, e, ...)    _LL_ENUM_NEXT(id)   (e, ##__VA_ARGS__)


LL_DECL(ptr, void *);
LL_DECL(str, char *);
LL_DECL(char, char);



#endif //_LINKED_LIST_H
