#ifndef _MAP_H
#define _MAP_H

#include <stdint.h>
#include <stdbool.h>

#include "linked_list.h"

// Symbols

#define _MAP(id)                map__##id
#define _MAP__(id)              map__##id##__
#define _MAP_PTR(id)            map__##id##_ptr
#define _MAP_K(id)              map__##id##_K
#define _MAP_V(id)              map__##id##_V
#define _MAP_KVP(id)            map__##id##_kvp
#define _MAP_KVP__(id)          map__##id##_kvp__
#define _MAP_LL_ID(id)          map__##id##_ll
#define _MAP_HASH(id)           map__##id##_hash
#define _MAP_NEW(id)            map__##id##_new
#define _MAP_FREE(id)           map__##id##_free
#define _MAP_PUT(id)            map__##id##_put
#define _MAP_GET(id)            map__##id##_get
#define _MAP_CONTAINS(id)       map__##id##_contains
#define _MAP_REMOVE(id)         map__##id##_remove
#define _MAP_CLEAR(id)          map__##id##_clear

#define _MAP_NOOP    NOOP_FUNC
#define _MAP_buckets _buckets
#define _MAP_hashCount _hashCount

// DEF (public)

#define _MAP_DEF(id) struct _MAP__(id) { \
    LL(_MAP_LL_ID(id)) (*_MAP_buckets)[]; \
    size_t _MAP_hashCount; \
    size_t count; \
    _MAP_HASH(id) *hash; \
    _MAP_V(id) def; \
}
#define _MAP_DEF_KVP(id) struct _MAP_KVP__(id) { \
    _MAP_K(id) key; \
    _MAP_V(id) value; \
}

// DECL

#define _MAP_DECL(id)               typedef struct _MAP__(id) _MAP(id); _MAP_DEF(id)
#define _MAP_DECL_PTR(id)           typedef _MAP(id) *_MAP_PTR(id)
#define _MAP_DECL_K(id, K)          typedef K _MAP_K(id)
#define _MAP_DECL_V(id, V)          typedef V _MAP_V(id)
#define _MAP_DECL_KVP(id)           typedef struct _MAP_KVP__(id) _MAP_KVP(id); _MAP_DEF_KVP(id)
#define _MAP_DECL_HASH__(name, T)   size_t name(T key)
#define _MAP_DECL_HASH(id)          typedef _MAP_DECL_HASH__(_MAP_HASH(id), _MAP_K(id))
#define _MAP_DECL_NEW(id)           _MAP_PTR(id) _MAP_NEW(id)(_MAP_V(id) def, _MAP_HASH(id) *hash, size_t hashCount)
#define _MAP_DECL_FREE(id)          void _MAP_FREE(id)(_MAP_PTR(id) m)
#define _MAP_DECL_PUT(id)           void _MAP_PUT(id)(_MAP_PTR(id) m, _MAP_K(id) key, _MAP_V(id) value)
#define _MAP_DECL_GET(id)           _MAP_V(id) _MAP_GET(id)(_MAP_PTR(id) m, _MAP_K(id) key)
#define _MAP_DECL_CONTAINS(id)      bool _MAP_CONTAINS(id)(_MAP_PTR(id) m, _MAP_K(id) key, _MAP_V(id) *out)
#define _MAP_DECL_REMOVE(id)        _MAP_V(id) _MAP_REMOVE(id)(_MAP_PTR(id) m, _MAP_K(id) key)
#define _MAP_DECL_CLEAR(id)         void _MAP_CLEAR(id)(_MAP_PTR(id) m)

// DEF (private)

#define _MAP_BUCKET(m, key) (&(*(m)->_MAP_buckets)[(m)->hash(key) % (m)->_MAP_hashCount])
#define _MAP_DEF_NEW(id) _MAP_DECL_NEW(id) { \
    _MAP_PTR(id) m = malloc(sizeof(_MAP(id))); \
    *m = (_MAP(id)) { \
        ._MAP_buckets = malloc(sizeof(LL(_MAP_LL_ID(id))) * hashCount), \
        ._MAP_hashCount = hashCount, \
        .count = 0, \
        .hash = hash, \
        .def = def \
    }; \
    for (size_t i = 0; i < m->_MAP_hashCount; i++) (*m->_MAP_buckets)[i] = ll_instance(_MAP_LL_ID(id), (_MAP_KVP(id)) { .value = m->def }); \
    return m; \
} _MAP_NOOP
#define _MAP_DEF_FREE(id) _MAP_DECL_FREE(id) { \
    _MAP_CLEAR(id)(m); \
    free(m->_MAP_buckets); \
    free(m); \
} _MAP_NOOP
#define _MAP_DEF_PUT(id) _MAP_DECL_PUT(id) { \
    LL(_MAP_LL_ID(id)) *bucket = _MAP_BUCKET(m, key); \
    LL_ENUM(_MAP_LL_ID(id)) e = ll_enumerator(_MAP_LL_ID(id), bucket); \
    while (ll_enum_next(_MAP_LL_ID(id), &e)) { \
        if (e.current->key != key) continue; \
        e.current->value = value; \
        return; \
    } \
    ll_add(_MAP_LL_ID(id), bucket, (_MAP_KVP(id)) { .key = key, .value = value }); \
    m->count++; \
} _MAP_NOOP
#define _MAP_DEF_GET(id) _MAP_DECL_GET(id) { \
    LL_ENUM(_MAP_LL_ID(id)) e = ll_enumerator(_MAP_LL_ID(id), _MAP_BUCKET(m, key)); \
    while (ll_enum_next(_MAP_LL_ID(id), &e)) { \
        if (e.current->key == key) return e.current->value; \
    } \
    return m->def; \
} _MAP_NOOP
#define _MAP_DEF_CONTAINS(id) _MAP_DECL_CONTAINS(id) { \
    LL_ENUM(_MAP_LL_ID(id)) e = ll_enumerator(_MAP_LL_ID(id), _MAP_BUCKET(m, key)); \
    while (ll_enum_next(_MAP_LL_ID(id), &e)) if (e.current->key == key) { \
        if (out) *out = e.current->value; \
        return true; \
    } \
    return false; \
} _MAP_NOOP
#define _MAP_DEF_REMOVE(id) _MAP_DECL_REMOVE(id) { \
    LL(_MAP_LL_ID(id)) *bucket = _MAP_BUCKET(m, key); \
    LL_ENUM(_MAP_LL_ID(id)) e = ll_enumerator(_MAP_LL_ID(id), bucket); \
    while (ll_enum_next(_MAP_LL_ID(id), &e)) { \
        if (e.current->key != key) continue; \
        _MAP_V(id) removed = e.current->value; \
        ll_remove(_MAP_LL_ID(id), bucket, e.index); \
        m->count--; \
        return removed; \
    } \
    return m->def; \
} _MAP_NOOP
#define _MAP_DEF_CLEAR(id) _MAP_DECL_CLEAR(id) { \
    for (size_t i = 0; i < m->_MAP_hashCount; i++) ll_clear(_MAP_LL_ID(id), &(*(m)->_buckets)[i]); \
    m->count = 0; \
} _MAP_NOOP

// API

#define MAP_DECL_TYPES(id, K, V) \
    _MAP_DECL_K(id, K); \
    _MAP_DECL_V(id, V); \
    _MAP_DECL_KVP(id); \
    LL_DECL_TYPES(_MAP_LL_ID(id), _MAP_KVP(id)); \
    _MAP_DECL_HASH(id); \
    _MAP_DECL(id); \
    _MAP_DECL_PTR(id)
#define MAP_DECL_FUNCS(id) \
    LL_DECL_FUNCS(_MAP_LL_ID(id)); \
    _MAP_DECL_NEW(id); \
    _MAP_DECL_FREE(id); \
    _MAP_DECL_PUT(id); \
    _MAP_DECL_GET(id); \
    _MAP_DECL_CONTAINS(id); \
    _MAP_DECL_REMOVE(id); \
    _MAP_DECL_CLEAR(id)
#define MAP_DECL(id, K, V) \
    MAP_DECL_TYPES(id, K, V); \
    MAP_DECL_FUNCS(id)
#define MAP_DEF(id) \
    LL_DEF(_MAP_LL_ID(id)); \
    _MAP_DEF_NEW(id); \
    _MAP_DEF_FREE(id); \
    _MAP_DEF_PUT(id); \
    _MAP_DEF_GET(id); \
    _MAP_DEF_CONTAINS(id); \
    _MAP_DEF_REMOVE(id); \
    _MAP_DEF_CLEAR(id)
#define MAP(id)                                     _MAP_PTR(id)
#define MAP_DECL_HASH(name, T)                      _MAP_DECL_HASH__(name, T)
#define map_new(id, def, hashFunc, hashCount)       _MAP_NEW(id)        (def, hashFunc, hashCount)
#define map_free(id, m, ...)                        _MAP_FREE(id)       (m, ##__VA_ARGS__)
#define map_put(id, m, ...)                         _MAP_PUT(id)        (m, ##__VA_ARGS__)
#define map_get(id, m, ...)                         _MAP_GET(id)        (m, ##__VA_ARGS__)
#define map_contains(id, m, ...)                    _MAP_CONTAINS(id)   (m, ##__VA_ARGS__)
#define map_remove(id, m, ...)                      _MAP_REMOVE(id)     (m, ##__VA_ARGS__)
#define map_clear(id, m, ...)                       _MAP_CLEAR(id)      (m, ##__VA_ARGS__)


MAP_DECL_HASH(map_hash_char, char);
MAP_DECL_HASH(map_hash_wchar, wchar_t);
MAP_DECL_HASH(map_hash_uint, unsigned int);
MAP_DECL_HASH(map_hash_ulong, unsigned long);
MAP_DECL_HASH(map_hash_str, const char *);


#endif //_MAP_H
