#include "board_loader.h"

#include <stdlib.h>
#include <dlfcn.h>

#include "avrsimv2.h"
#include "log.h"

#define _str(x) #x
#define str(x) _str(x)

static void *loadSO(void *dl_handle, char *name) {
    char *error;
    void *func = dlsym(dl_handle, name);
    if ((error = dlerror())) {
        L_ERROR("Could not load symbol \"%s\": %s", name, error);
        return NULL;
    }
    return func;
}

//static BOARD_FUNC_AVR_INIT   (dummy_board_avr_init__, board, avr) {}
static BOARD_FUNC_GUI_INIT   (dummy_board_gui_init__, board) {}
static BOARD_FUNC_GUI_GETSIZE(dummy_board_gui_getSize__, board) { return GLvec2f_ZERO; }
static BOARD_FUNC_GUI_DRAW   (dummy_board_gui_draw__, board) {}
static BOARD_FUNC_GUI_ONKEY  (dummy_board_gui_onKey__, board, key, scancode, action, mods) {}
static BOARD_FUNC_CLOSE      (dummy_board_close__, board) {}

typedef BOARD_LOAD_F(, , );

board_t *board_load(avrsimv2_t *s, char *path, int argc, char *argv[]) {
    void *dl_handle = dlopen(path, RTLD_NOW);
    if (!dl_handle) {
        L_ERROR("%s", dlerror());
        return NULL;
    }
    
    BOARD_LOAD_F_NAME *board_load_f = loadSO(dl_handle, str(BOARD_LOAD_F_NAME));
    if (!board_load_f) return NULL;
    
    board_t *board = malloc(sizeof(board_t));
    memset(board, 0, sizeof(board_t));
    board->s = s;
    board->logger = logger;
    board->dl_handle = dl_handle;
    
    char **tmp = malloc(sizeof(char *) * (argc + 1));
    tmp[0] = path;
    memcpy(tmp + 1, argv, sizeof(char *) * argc);
    board_load_f(board, argc + 1, tmp);
    free(tmp);
    
    if (!board->avr.init) L_ERROR("Boards must define avr.init");
    if (!board->gui.init) board->gui.init = dummy_board_gui_init__;
    if (!board->gui.getSize) board->gui.getSize = dummy_board_gui_getSize__;
    if (!board->gui.draw) board->gui.draw = dummy_board_gui_draw__;
    if (!board->gui.onKey) board->gui.onKey = dummy_board_gui_onKey__;
    if (!board->close) board->close = dummy_board_close__;
    
    return board;
}
void board_unload(board_t **board) {
    L_DEBUG("Closing board...");
    (*board)->close(*board);
    L_DEBUG("Closing dl...");
    dlclose((*board)->dl_handle);
    free(*board);
    *board = NULL;
}
